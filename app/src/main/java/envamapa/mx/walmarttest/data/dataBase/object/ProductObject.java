package envamapa.mx.walmarttest.data.dataBase.object;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ProductObject extends RealmObject {

    @PrimaryKey
    private String skuId;
    private String skuDisplayNameText;
    private String department;
    private String basePrice;
    private String codeMessage;
    private String longDescription;

    public ProductObject() {
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getSkuDisplayNameText() {
        return skuDisplayNameText;
    }

    public void setSkuDisplayNameText(String skuDisplayNameText) {
        this.skuDisplayNameText = skuDisplayNameText;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(String basePrice) {
        this.basePrice = basePrice;
    }

    public String getCodeMessage() {
        return codeMessage;
    }

    public void setCodeMessage(String codeMessage) {
        this.codeMessage = codeMessage;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }
}
