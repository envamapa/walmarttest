package envamapa.mx.walmarttest.data;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Iterator;

import envamapa.mx.walmarttest.R;
import envamapa.mx.walmarttest.commons.Constants;
import envamapa.mx.walmarttest.data.dataBase.MyRealm;
import envamapa.mx.walmarttest.data.dataBase.model.ProductModel;
import envamapa.mx.walmarttest.data.dataBase.object.ProductObject;
import envamapa.mx.walmarttest.data.sources.service.Request;
import envamapa.mx.walmarttest.data.sources.service.RespProduct.Product;
import io.realm.RealmChangeListener;
import io.realm.RealmModel;
import io.realm.RealmResults;

public class DataInteractor {

    public DataInteractor (){

    }

    public interface dataInteractorListener {
        void onSuccess(Object object);

        void onError(String message);
    }

    /**
     * INVOCAR EL SERVICIO PARA OBTENER LA INFORMACIÓN DEL PRODUCTO
     * */
    public void getProductInformation(final Context context, final dataInteractorListener listener){
        String url = Constants.SERVER+Constants.CAT;
        Request.getRequestInstance().doRequest(context, url, new Request.serviceCallStatus() {
            @Override
            public void onSuccess(JSONObject jsonObject) {
                Product product = new Gson().fromJson(jsonObject.toString(), Product.class);
                if(product != null && product.getCodeMessage().equalsIgnoreCase(Constants.CODE_ZERO)){
                    saveProductInformation(product);
                    listener.onSuccess(product);
                }else{
                    //Si el código de respuesta es cero o la respuesta es nula
                    listener.onError(context.getString(R.string.server_error)+"\nCode response: "+product.getCodeMessage());
                }
            }

            @Override
            public void onFailed(String mensaje) {

            }
        });
    }

    /**
     * ALMACENAMOS LA INFORMACIÓN EN BASE DE DATOS SI ES QUE LA APP SE QUIERE UTILIZAR OFFLINE
     * */
    private void saveProductInformation(Product product){
        ProductObject object = new ProductObject();

        object.setBasePrice(product.getBasePrice());
        object.setCodeMessage(product.getCodeMessage());
        object.setDepartment(product.getDepartment());
        object.setLongDescription(product.getLongDescription());
        object.setSkuDisplayNameText(product.getSkuDisplayNameText());
        object.setSkuId(product.getSkuId());

        ProductModel model = new ProductModel();
        model.insert(object);
    }

    /**
     * CONSULTAR LA INFORMACIÓN DEL PRODUCTO QUE SE ALMACENÓ EN BASE DE DATOS
     * */
    public void getProductInformation(final dataInteractorListener listener){
        ProductModel model = new ProductModel();
        model.getProduct().addChangeListener(new RealmChangeListener<ProductObject>() {
            @Override
            public void onChange(ProductObject result) {
                if(result.isLoaded()){
                    Product product = new Product();
                    product.setBasePrice(result.getBasePrice());
                    product.setCodeMessage(result.getCodeMessage());
                    product.setDepartment(result.getDepartment());
                    product.setLongDescription(result.getLongDescription());
                    product.setSkuDisplayNameText(result.getSkuDisplayNameText());
                    product.setSkuId(result.getSkuId());

                    listener.onSuccess(product);
                }else{
                    listener.onError("Producto no encontrado");
                }
            }
        });
    }

    /**
     * CONSULTAMOS SI EXISTEN REGISTROS EN LA BASE DE DATOS
     * */
    public void productExists(final dataInteractorListener listener){
        ProductModel model = new ProductModel();
        if(MyRealm.existDB() && model.productExists()){
            listener.onSuccess("true");
        }else{
            listener.onSuccess("false");
        }
    }
}
