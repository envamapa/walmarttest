package envamapa.mx.walmarttest.data.sources.service;

import android.content.Context;

import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;

import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;

public class SSLFactory {

    private StringBuilder camposConError;
    private Context context;
    private HurlStack hurlStack;
    //private Common common;

    public SSLFactory(Context context) {
        this.context = context;
    }

    public HttpStack newSslSocketFactory() {
        try {
            BasicHttpParams params = new BasicHttpParams();
            DefaultHttpClient httpClient = new DefaultHttpClient(params);
            HttpStack httpStack = new HttpClientStack(httpClient);

            return httpStack;
        } catch (Exception e) {
            System.out.println("sslDefaultHttpClientSocketFactory Exception:_  " + e.toString());
            throw new AssertionError(e);
        }
    }
}
