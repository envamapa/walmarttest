package envamapa.mx.walmarttest.data.dataBase.model;

import envamapa.mx.walmarttest.commons.Utils;
import envamapa.mx.walmarttest.data.dataBase.object.ProductObject;
import envamapa.mx.walmarttest.data.sources.service.RespProduct.Product;
import io.realm.Realm;
import io.realm.RealmResults;

public class ProductModel {

    private Realm realm;

    public ProductModel() {
        realm = Realm.getDefaultInstance();
    }

    /**
     * FUNCIÓN PARA INSERTAR UNA TUPLA
     */
    public void insert(ProductObject object) {
        realm.beginTransaction();
        realm.insertOrUpdate(object);
        realm.commitTransaction();
    }

    /**
     * CONSULTAR EL PRODUCTO DE LA BASE DE DATOS
     * */
    public ProductObject getProduct(){
        ProductObject result = realm
                .where(ProductObject.class)
                .findFirstAsync();
        return result;
    }

    /**
     * CONSULTAR SI HAY REGISTROS EN LA BASE DE DATOS
     * */
    public boolean productExists(){
        Long total = realm
                .where(ProductObject.class)
                .count();
        return total > 0;
    }

}
