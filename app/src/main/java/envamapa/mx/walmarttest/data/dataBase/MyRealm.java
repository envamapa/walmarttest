package envamapa.mx.walmarttest.data.dataBase;

import envamapa.mx.walmarttest.commons.Utils;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MyRealm {

    public static boolean existDB() {
        boolean result;
        Realm realm = Realm.getDefaultInstance();
        if (realm.isEmpty()) {
            result = false;
        } else {
            result = true;
        }

        return result;
    }
}

