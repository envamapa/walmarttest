package envamapa.mx.walmarttest.data.sources.service;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import envamapa.mx.walmarttest.R;
import envamapa.mx.walmarttest.commons.Constants;
import envamapa.mx.walmarttest.commons.Utils;

public class Request {

    private VolleyHelper volley;
    protected RequestQueue fRequestQueue;
    private static Request requestInstance;

    /**
     * Factory de Request
     *
     * @return requestInstance
     */
    public static Request getRequestInstance() {
        if (requestInstance == null) {
            requestInstance = new Request();
        }
        return requestInstance;
    }

    /**
     * INTERFACE DE RESPUESTA DE SERVICIO
     */
    public interface serviceCallStatus {
        void onSuccess(JSONObject jsonObject);

        void onFailed(String mensaje);
    }

    /**
     * HACER REQUEST AL SERVIDOR
     * */
    public void doRequest(final Context context, String url, final serviceCallStatus serviceCallStatus){
        Utils.printLogInfo("URL: " + url, true, false);
        if (Utils.connectedToInternet(context)) {
            final JsonObjectRequest request = new JsonObjectRequest(com.android.volley.Request.Method.GET, url, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {
                    Utils.printLogInfo(jsonObject.toString(), true, true);
                    serviceCallStatus.onSuccess(jsonObject);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.printLogError(error.toString(), true, false);
                    if (error instanceof TimeoutError) {
                        serviceCallStatus.onFailed(context.getString(R.string.error_number_intents));
                        Utils.printLogError("LIMITE DE INTENTOS EXCEDIDO: " + error.toString(), true, false);
                    } else if (error instanceof NoConnectionError) {
                        serviceCallStatus.onFailed(context.getString(R.string.error_verify_internet_conection));
                        Utils.printLogError("SIN CONEXIÒN A INTERNET: " + error.toString(), true, false);
                    } else if (error instanceof ServerError) {
                        serviceCallStatus.onFailed(context.getString(R.string.server_error));
                        Utils.printLogError("ERROR DEL SERVIDOR: " + error.toString(), true, false);
                    } else {
                        serviceCallStatus.onFailed(context.getString(R.string.system_error));
                        Utils.printLogError("ERROR DESCONOCIDO: " + error.toString(), true, false);
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Accept", "application/json");
                    return params;
                }
            };
            volley = VolleyHelper.getInstance(context);
            fRequestQueue = volley.getHttpRequestQueue();
            if (fRequestQueue == null) {
                fRequestQueue = volley.getHttpRequestQueue();
            }
            request.setRetryPolicy(new DefaultRetryPolicy(
                    Constants.TIMEOUT, Constants.RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            );
            request.setTag(url);

            fRequestQueue.add(request);
        } else {
            Utils.printLogError("SIN CONEXIÒN A INTERNET", true, false);
            serviceCallStatus.onFailed(context.getString(R.string.error_verify_internet_conection));
        }
    }
}
