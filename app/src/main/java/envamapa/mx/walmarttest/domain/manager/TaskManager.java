package envamapa.mx.walmarttest.domain.manager;

import javax.inject.Inject;

import envamapa.mx.walmarttest.domain.taskInterface.Task;

public class TaskManager {
    private static TaskManager taskManager;

    @Inject
    public TaskManager(){
    }

    public void execute(Task task){
        task.execute();
    }
}
