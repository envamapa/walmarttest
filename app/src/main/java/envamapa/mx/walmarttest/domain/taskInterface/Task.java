package envamapa.mx.walmarttest.domain.taskInterface;

public interface Task {

    public abstract void execute();

}