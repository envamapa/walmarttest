package envamapa.mx.walmarttest.domain.task;

import android.content.Context;

import javax.inject.Inject;

import envamapa.mx.walmarttest.commons.Constants;
import envamapa.mx.walmarttest.data.DataInteractor;
import envamapa.mx.walmarttest.data.sources.service.RespProduct.Product;
import envamapa.mx.walmarttest.domain.taskInterface.Task;

public class GetProductInformationTask implements Task {

    //Declaración de variables
    DataInteractor dataInteractor;
    getProductInformationTaskListener listener;
    Context context;

    @Inject
    Constants constants;

    public interface getProductInformationTaskListener{
        void onFinishTask(Product product);
        void onErrorTask(String resp);
    }

    public GetProductInformationTask(Context context, getProductInformationTaskListener listener){
        this.context = context;
        this.listener = listener;
    }

    @Override
    public void execute() {
        dataInteractor = new DataInteractor();
        dataInteractor.getProductInformation(context, new DataInteractor.dataInteractorListener() {
            @Override
            public void onSuccess(Object object) {
                listener.onFinishTask((Product) object);
            }

            @Override
            public void onError(String message) {
                listener.onErrorTask(message);
            }
        });
    }
}
