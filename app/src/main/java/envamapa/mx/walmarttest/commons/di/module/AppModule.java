package envamapa.mx.walmarttest.commons.di.module;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import envamapa.mx.walmarttest.domain.manager.TaskManager;

@Module
public class AppModule {

    Context context;

    public AppModule(Context context){
        this.context = context;
    }

    @Provides
    @Singleton
    public Context context(){
        return this.context;
    }

    @Provides
    @Singleton
    public Application application(){
        return (Application) context;
    }

    @Provides
    @Singleton
    public TaskManager taskManager(){
        return new TaskManager();
    }

}
