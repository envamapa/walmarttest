package envamapa.mx.walmarttest.commons.di.component;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Component;
import envamapa.mx.walmarttest.view.MainActivity;
import envamapa.mx.walmarttest.commons.di.module.AppModule;
import envamapa.mx.walmarttest.view.MainPresenter;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(Application application);

    void inject(MainActivity mainActivity);

    void inject(MainPresenter presenter);

}
