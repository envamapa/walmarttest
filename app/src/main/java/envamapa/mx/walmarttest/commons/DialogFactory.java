package envamapa.mx.walmarttest.commons;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;

public class DialogFactory {

    private static Dialog dialog;
    private static String message;
    private static boolean isDialogActive;



    public static void createInstance(Context context, String mensaje) {
        dismissCurrentInstance();
        DialogFactory.message = mensaje;
        dialog = ProgressDialog.show(context, "Por favor espere...", mensaje + "...", true);
        dialog.setCancelable(false);
        isDialogActive = true;
        show();
    }

    public static void dismissCurrentInstance() {
        dismissOldInstance();
        isDialogActive = false;
    }

    protected static void dismissOldInstance() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
                //isDialogActive = false;
            }
        }
    }

    public static boolean shown(){
        if(dialog.isShowing())
            return true;
        else
            return false;
    }

    private static void show() {
        isDialogActive = true;
        dialog.show();
    }

    protected static void reloadLastInstance(Context context){
        if (isDialogActive) {
            createInstance(context, message);
        }
    }

}