package envamapa.mx.walmarttest.commons;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.orhanobut.logger.Logger;

public class Utils {

    /**
     * IMPRIMIR LOG DE DEBUG
     *
     * @param message    Mensaje que se mostrara en el log.
     * @param jsonMobe   true: Es JSON <br> false: No es JSON
     * @param prettyMode Mostrar o no formateado.
     */
    public static void printLogDebug(String message, boolean prettyMode, boolean jsonMobe) {
        if (Constants.ALLOWING_LOGS) {
            if (jsonMobe) {
                Logger.json(message);
            } else {
                if (prettyMode) {
                    Logger.d(message);
                } else {
                    Log.d(Constants.TAG_LOG, message);
                }
            }
        }
    }

    /**
     * IMPRIMIR LOG DE INFO
     *
     * @param message    Mensaje que se mostrara en el log.
     * @param jsonMobe   true: Es JSON <br> false: No es JSON
     * @param prettyMode Mostrar o no formateado.
     */
    public static void printLogInfo(final String message, final boolean prettyMode, final boolean jsonMobe) {
        if (Constants.ALLOWING_LOGS) {
            if (jsonMobe) {
                Logger.json(message);
            } else {
                if (prettyMode) {
                    Logger.i(message);
                } else {
                    Log.i(Constants.TAG_LOG, message);
                }
            }
        }
    }

    /**
     * IMPRIMIR LOG DE ERROR
     *
     * @param message    Mensaje que se mostrara en el log.
     * @param jsonMobe   true: Es JSON <br> false: No es JSON
     * @param prettyMode Mostrar o no formateado.
     */
    public static void printLogError(final String message, final boolean prettyMode, final boolean jsonMobe) {
        if (Constants.ALLOWING_LOGS) {
            if (jsonMobe) {
                Logger.json(message);
            } else {
                if (prettyMode) {
                    Logger.e(message);
                } else {
                    Log.e(Constants.TAG_LOG, message);
                }
            }
        }
    }

    /**
     * VERIFICA LA CONEXIÓN A INTERNET
     * */
    public static boolean connectedToInternet(Context context){
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

}
