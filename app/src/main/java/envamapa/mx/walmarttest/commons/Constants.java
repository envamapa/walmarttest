package envamapa.mx.walmarttest.commons;

public class Constants {

    public static final boolean ALLOWING_LOGS = true;
    public static final String TAG_LOG = "Walmart Test";

    public static final int TIMEOUT = 20000;
    public static final int RETRIES = 3;

    public static final String SERVER = "https://super.walmart.com.mx/api/rest/model/atg/commerce/";
    public static final String CAT = "catalog/ProductCatalogActor/getSkuSummaryDetails?storeId=0000009999&upc=00750129560012&skuId=00750129560012";

    public static final String CODE_ZERO = "0";
}
