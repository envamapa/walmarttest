package envamapa.mx.walmarttest.commons;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import envamapa.mx.walmarttest.commons.di.component.AppComponent;
import envamapa.mx.walmarttest.commons.di.component.DaggerAppComponent;
import envamapa.mx.walmarttest.commons.di.module.AppModule;
import io.realm.Realm;

public class Application extends android.app.Application {

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        createDaggerInjections();
        Logger.addLogAdapter(new AndroidLogAdapter());
    }

    /**
     * CREAR EL MODULO DE INYECCION DE DAGGER2
     */
    private void createDaggerInjections(){
        mAppComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        mAppComponent.inject(this);
    }

    public AppComponent getAppComponent(){
        return this.mAppComponent;
    }
}
