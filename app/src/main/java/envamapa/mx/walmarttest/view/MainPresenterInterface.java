package envamapa.mx.walmarttest.view;

public interface MainPresenterInterface {
    void verifyPermissions();

    void enableOfflineBtn();

    void getProductInformation(boolean queryOnline);
}
