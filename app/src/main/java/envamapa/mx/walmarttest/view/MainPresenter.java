package envamapa.mx.walmarttest.view;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import javax.inject.Inject;

import envamapa.mx.walmarttest.commons.Application;
import envamapa.mx.walmarttest.commons.Utils;
import envamapa.mx.walmarttest.data.DataInteractor;
import envamapa.mx.walmarttest.data.sources.service.RespProduct.Product;
import envamapa.mx.walmarttest.domain.manager.TaskManager;
import envamapa.mx.walmarttest.domain.task.GetProductInformationTask;

public class MainPresenter implements MainPresenterInterface {

    //Declaración de variables
    MainActivityInterface viewInterface;
    Application application;
    DataInteractor dataInteractor;
    Activity activity;
    private static String[] PERMISSIONS_READ_WRITE = {Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static int REQUEST_READ_WRITE = 1;

    @Inject
    TaskManager taskManager;

    public MainPresenter(MainActivityInterface viewInterface, Activity activity, Application application){
        this.viewInterface = viewInterface;
        this.application = application;
        this.activity = activity;
        dataInteractor = new DataInteractor();
        application.getAppComponent().inject(this);
    }

    /**
     * VERIFICAR SI LA APLICACIÓN TIENE LOS PERMISOS NECESARIOS
     * */
    @Override
    public void verifyPermissions(){
        if ((ActivityCompat.checkSelfPermission(application.getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)

                || (ActivityCompat.checkSelfPermission(application.getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)

                ) {

            requestWriteReadPermission();
        }
    }

    private void requestWriteReadPermission() {
        ActivityCompat.requestPermissions(activity, PERMISSIONS_READ_WRITE, REQUEST_READ_WRITE);
    }

    @Override
    public void enableOfflineBtn(){
        dataInteractor.productExists(new DataInteractor.dataInteractorListener() {
            @Override
            public void onSuccess(Object object) {
                viewInterface.enableOfflineBtn(Boolean.parseBoolean((String)object));
            }

            @Override
            public void onError(String message) {

            }
        });
    }

    @Override
    public void getProductInformation(boolean queryOnline){
        viewInterface.showDialogLoading();
        //Se consulta desde el servicio
        if(queryOnline){
            getProductInformationOnline();
        }else{ //Se consulta en base de datos
            getProductInformationOffline();
        }
    }

    private void getProductInformationOnline(){
        taskManager.execute(new GetProductInformationTask(application.getApplicationContext(), new GetProductInformationTask.getProductInformationTaskListener() {
            @Override
            public void onFinishTask(Product product) {
                viewInterface.dismissDialogLoading();
                viewInterface.setProductInformation(product);
            }

            @Override
            public void onErrorTask(String resp) {
                viewInterface.dismissDialogLoading();
                viewInterface.showError(resp);
            }
        }));
    }

    private void getProductInformationOffline(){
        dataInteractor.getProductInformation(new DataInteractor.dataInteractorListener() {
            @Override
            public void onSuccess(Object object) {
                viewInterface.dismissDialogLoading();
                viewInterface.setProductInformation((Product) object);
            }

            @Override
            public void onError(String message) {
                viewInterface.dismissDialogLoading();
                viewInterface.showError(message);
            }
        });
    }
}
