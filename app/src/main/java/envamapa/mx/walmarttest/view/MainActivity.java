package envamapa.mx.walmarttest.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import envamapa.mx.walmarttest.R;
import envamapa.mx.walmarttest.commons.Application;
import envamapa.mx.walmarttest.commons.DialogFactory;
import envamapa.mx.walmarttest.commons.Utils;
import envamapa.mx.walmarttest.data.sources.service.RespProduct.Product;

public class MainActivity extends AppCompatActivity implements MainActivityInterface, View.OnClickListener {

    //Declaración de variables
    private Button onlineBtn;
    private Button offlineBtn;
    private TextView titleTxt;
    private LinearLayout productName;
    private LinearLayout department;
    private LinearLayout skuId;
    private LinearLayout basePrice;
    private LinearLayout description;
    private TextView productNameTxt;
    private TextView departmentTxt;
    private TextView skuIdTxt;
    private TextView basePriceTxt;
    private TextView descriptionTxt;

    //Presentador
    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MainPresenter(this, this, ((Application)getApplication()));
        presenter.verifyPermissions();

        initView();
        initData();
    }

    /**
     * INICIALIZAMOS LOS ELEMENTOS DE LA VISTA
     * */
    private void initView(){
        onlineBtn = findViewById(R.id.onlineBtn);
        offlineBtn = findViewById(R.id.offlineBtn);

        productName = findViewById(R.id.productName);
        department = findViewById(R.id.department);
        skuId = findViewById(R.id.skuId);
        basePrice = findViewById(R.id.basePrice);
        description = findViewById(R.id.description);

        titleTxt = findViewById(R.id.title);
        productNameTxt = findViewById(R.id.productNameTxt);
        departmentTxt = findViewById(R.id.departmentTxt);
        skuIdTxt = findViewById(R.id.skuIdTxt);
        basePriceTxt = findViewById(R.id.basePriceTxt);
        descriptionTxt = findViewById(R.id.descriptionTxt);

        onlineBtn.setOnClickListener(this);
        offlineBtn.setOnClickListener(this);
    }

    /**
     * VERIFICAMOS SI PODEMOS HABILITAR EL BOTÓN DE OFFLINE
     * */
    private void initData(){
        presenter.enableOfflineBtn();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.onlineBtn:
                presenter.getProductInformation(true);
                titleTxt.setText(getString(R.string.online));
                break;
            case R.id.offlineBtn:
                presenter.getProductInformation(false);
                titleTxt.setText(getString(R.string.offline));
                break;
        }
    }

    @Override
    public void enableOfflineBtn(boolean enable){
        offlineBtn.setEnabled(enable);
    }

    /**
     * PONER LA INFORMACIÓN DEL PRODUCTO EN LA PANTALLA
     * */
    @Override
    public void setProductInformation(Product product){
        productName.setVisibility(View.VISIBLE);
        department.setVisibility(View.VISIBLE);
        skuId.setVisibility(View.VISIBLE);
        basePrice.setVisibility(View.VISIBLE);
        description.setVisibility(View.VISIBLE);

        productNameTxt.setText(product.getSkuDisplayNameText());
        departmentTxt.setText(product.getDepartment());
        skuIdTxt.setText(product.getSkuId());
        basePriceTxt.setText(product.getBasePrice());
        descriptionTxt.setText(product.getLongDescription());

        presenter.enableOfflineBtn();
    }

    /**
     * Muestra el dialogo de carga
     */
    @Override
    public void showDialogLoading() {
        try {
            DialogFactory.createInstance(this, getResources().getString(R.string.processing));
        } catch (Exception e) {
            Utils.printLogError(e.getMessage(), true, false);
        }
    }

    /**
     * Oculta el dialogo de carga
     */
    @Override
    public void dismissDialogLoading() {
        try {
            DialogFactory.dismissCurrentInstance();
        } catch (Exception e) {
            Utils.printLogError(e.getMessage(), true, false);
        }
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
