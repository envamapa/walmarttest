package envamapa.mx.walmarttest.view;

import envamapa.mx.walmarttest.data.sources.service.RespProduct.Product;

public interface MainActivityInterface {
    void enableOfflineBtn(boolean enable);

    void setProductInformation(Product product);

    void showDialogLoading();

    void dismissDialogLoading();

    void showError(String message);
}
